# Sundry

The stuffs that doesn't fit into the other categories goes here.


## What is this repo for ? 
* We shall discuss the *political* and *economical* aspects of the project here. 
* We can upload the *Artworks* and any other *original media* related to this project. 
* We shall host a wiki in here that tracks and documents the works done, WIP, progress, e.t.c. 

